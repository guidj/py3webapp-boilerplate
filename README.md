#Py3 WebApp Boilerplate
 
A Python3 web app boilerplate. This is the folder structure I use when I develop python web apps. Default framework is Flask.


##Usage

###Building Environment

```sh
bash app/rsc/scripts/build-env.sh
```

###Behavioural Tests
Behave is used as a behavioural testing framework. To execute tests, run:

```sh 
bash app/rsc/scripts/behavioural-tests.sh
```

###Unit Tests
```sh
bash app/rsc/scripts/unit-tests.sh
```
