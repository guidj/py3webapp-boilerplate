#!/bin/bash

function behaviouralTests(){

    SOURCE="${BASH_SOURCE[0]}"

    while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
      DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
      SOURCE="$(readlink "$SOURCE")"
      [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    done
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

    cd ${DIR}/../../../
    PROJECTNAME=$(basename `pwd`)
    ENV=${PROJECTNAME}-env
    PYTHONENV=./${ENV}/bin
    TESTDIR=app/tests/behavioural
    SRCROOT=./app/src

    export PYTHONPATH=${PYTHONPATH}:${SRCROOT}
    ${PYTHONENV}/activate
    ${PYTHONENV}/behave ${TESTDIR}


}

behaviouralTests
